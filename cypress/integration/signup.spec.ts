describe('Signup Page', () => {

    beforeEach(() => {
        cy.visit('/');
    })

    it('should have signup title', () => {

        cy.contains('Sign up');

    })

    it('should have a form', () => {

        cy.get('.signup-form').should('be.visible');

    })

    it('should have a form with valid form fields', () => {

        cy.get('input[placeholder="First Name"]').invoke('attr', 'placeholder')
            .should('contain', 'First Name');

        cy.get('input[placeholder="Last Name"]').invoke('attr', 'placeholder')
            .should('contain', 'Last Name');

        cy.get('input[placeholder="Email"]').invoke('attr', 'placeholder')
            .should('contain', 'Email');

        cy.get('input[placeholder="Password"]').invoke('attr', 'placeholder')
            .should('contain', 'Password');

        cy.get('.button')
            .should('contain', 'Create Account');
    })

    it('Firstname should validate required', () => {

        cy.get('input[placeholder="First Name"]').click();

        cy.get('input[placeholder="First Name"]').blur();

        cy.get('.clear-button-errors').should('be.visible');

        cy.get('.clear-button-errors').contains('This is a required field');
    })

    it('Lastname should validate required', () => {

        cy.get('input[placeholder="Last Name"]').click();

        cy.get('input[placeholder="Last Name"]').blur();

        cy.get('.clear-button-errors').should('be.visible');

        cy.get('.clear-button-errors').contains('This is a required field');
    })

    it('Email should validate required', () => {

        cy.get('input[placeholder="Email"]').click();

        cy.get('input[placeholder="Email"]').blur();

        cy.get('.clear-button-errors').should('be.visible');

        cy.get('.clear-button-errors').contains('This is a required field');
    })

    it('Email should validate if address exists', () => {

        clickBlurElementAndType('Email', 'a@abc.com')

        cy.get('.clear-button-errors').contains('This email is in use');
    })

    it('Password should validate required', () => {

        const el = cy.get(`input[placeholder="Password"]`);

        el.click();

        el.blur();

        cy.get('.clear-button-errors').should('be.visible');

        cy.get('.clear-button-errors').contains('This is a required field');
    })

    it('Password should invalidate capital letters', () => {

        clickBlurElementAndType('Password', 'fooooooobar')

        cy.get('.clear-button-errors').contains('This password is in invalid');
    })

    it('Password should invalidate length', () => {

        clickBlurElementAndType('Password', 'Foobar');

        cy.get('.clear-button-errors').contains('This password is in invalid');
    })

    it('Form should validate if successful field values entered and spinner is visible', () => {

        clickBlurElementAndType('First Name', 'Murat');

        clickBlurElementAndType('Last Name', 'Zengin');

        clickBlurElementAndType('Email', 'murat@zensnet.com');

        clickBlurElementAndType('Password', 'FoooBarr');

        cy.get('.button').click();

        cy.get('.loading').should('be.visible');

        cy.get('.form-message').should('be.visible');

        cy.get('.form-message').contains('User Murat has been created successfully!');

    })

    it('Clear button clears the value', () => {

        cy.get(`input[placeholder="First Name"]`).click().type('Foo');

        cy.get('.clear').should('be.visible');

        cy.get('.clear').click({force: true});

        cy.get(`input[placeholder="First Name"]`).should('have.value', '');

    })

})

function clickBlurElementAndType(element: string, text?: any) {
    cy.get(`input[placeholder="${element}"]`).click().type(text).blur();
}
