# Fedex Signup

This is an Fedex assesment project. This project was generated
with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.
This project is hosted at https://zensnet@bitbucket.org/zensnet/fedexsignup.git

**How to get:**

git clone https://zensnet@bitbucket.org/zensnet/fedexsignup.git

**Email Validation**:

Beside async api validation, I have used the default angular email validation since
there no real accurate email validation exists.

Using regex
we can create hundreds of rules to cover different scenarios.
I believe, we as developers need a proper business rule delivered from
business to create a regex rule otherwise there is no. For this assessment, there is no reason
to reinvent the wheel thus I used angular built-in validation.

    Please use a@abc.com to trigger an async custom validation for an unhappy flow.

**Password Validation**:

This field uses a custom validation to check the correctness
of the given password.

    Password length should be minimum 8 chars

    Password should have at least one capital char

    Password should not contain first or last name

## Other Ideas

In addition to static forms creation, we could also implement
FormsArray and add the form fields and its substances along with
corresponding validation dynamically in the template.

Also we can implement NgRx or Redux to manage the state of the form.

For this specific assessment, I decided to go for current way.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Run

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you
change any of the source files.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `npm run test:c` to excute with coverage

## Running end-to-end tests

Run `npm run cypress:run` to execute the end-to-end tests via a platform of your choice. To use this command, you need
to first add a
package that implements end-to-end testing capabilities.
