import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {DebugElement} from "@angular/core";

describe('AppComponent', () => {
  let el: DebugElement,
      fixture: ComponentFixture<AppComponent>,
      component: AppComponent;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement;
  });

  it('should create the app', () => {

    expect(component).toBeTruthy();

  });

  it(`should have as title 'fedexSignup'`, () => {

    expect(component.title).toEqual('fedexSignup');

  });
});
