import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PasswordValidator} from "../../shared/validators/password.validator";
import {EmailValidator} from "../../shared/validators/email.validator";
import {AuthService} from "../../shared/services/auth.service";
import {User} from "../../shared/interfaces/user.interface";
import {FORM_FIELDS} from "../../shared/components/models/form-fields";
import {FormFields} from "../../shared/interfaces/form-fields";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent {
  message: string = '';
  loading: boolean = false;
  fields: FormFields = FORM_FIELDS;
  accountSignupForm: FormGroup = this.fb.group({
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', {
      validators: [Validators.required, Validators.email],
      asyncValidators: [this.emailValidator.checkEmail()],
      updateOn: 'blur'
    }],
    password: ['', [Validators.required, Validators.minLength(8), this.passwordValidator.checkStrength()]]
  }, {
    validators: [this.passwordValidator.checkPassword()],
  });

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private passwordValidator: PasswordValidator,
              private emailValidator: EmailValidator) {
  }

  onSubmit() {
    if (!this.accountSignupForm.valid) {
      return;
    }

    const user = this.accountSignupForm.value as User;

    this.loading = true;

    this.authService.sendSignupRequest(user).subscribe({
      next: (result) => {

        if (result) {

          this.message = `User ${result.firstName} has been created successfully!`;

        }

        this.loading = false;

      },
      error: () => {
        this.message = `Something went wrong!`;

        this.loading = false;
      }
    });
  }
}
