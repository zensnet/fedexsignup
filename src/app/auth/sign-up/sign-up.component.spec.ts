import {ComponentFixture, TestBed} from '@angular/core/testing';
import {SignUpComponent} from './sign-up.component';
import {AuthService} from "../../shared/services/auth.service";
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "../../shared/shared.module";
import {map, of, throwError} from "rxjs";
import {EmailValidator} from "../../shared/validators/email.validator";
import {PasswordValidator} from "../../shared/validators/password.validator";
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";
import {FORM_FIELDS} from "../../shared/components/models/form-fields";

function mockValidator(control: any) {
  const found = of(control.value === 'a@abc.com').pipe(
      map(result => result)
  )
  return found.pipe(
      map(result => {
        return result ? {emailExists: true} : null;
      })
  )
}

describe('SignUpComponent', () => {
  let component: SignUpComponent,
      authService: AuthService,
      fixture: ComponentFixture<SignUpComponent>,
      el: DebugElement,
      authServiceSpy: any;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['sendSignupRequest']);
    TestBed.configureTestingModule({
      declarations: [SignUpComponent],
      providers: [
        {
          provide: AuthService, useValue: authServiceSpy
        },
        EmailValidator, PasswordValidator],
      imports: [ReactiveFormsModule, FormsModule, HttpClientModule, SharedModule]

    })
        .compileComponents();

    authService = TestBed.get(AuthService);

    fixture = TestBed.createComponent(SignUpComponent);

    component = fixture.componentInstance;

    fixture.detectChanges();

    el = fixture.debugElement;
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate when all fields are entered correctly', () => {
    const email = component.accountSignupForm.controls[FORM_FIELDS.email.name];
    email.clearAsyncValidators();
    email.addAsyncValidators(mockValidator)

    component.accountSignupForm.setValue({
      firstName: 'qqqq',
      lastName: 'qqqq',
      email: 'qq@qq.com',
      password: 'kdkdkdWkdmkkddkdk.com'
    });

    expect(component.accountSignupForm.valid).toEqual(true);
  });

  it('should email control be invalid if exists, valid if does not exists', () => {
    const email = component.accountSignupForm.controls[FORM_FIELDS.email.name];
    email.clearAsyncValidators();
    email.addAsyncValidators(mockValidator)

    // Email exists
    email.setValue('a@abc.com');
    expect(email.valid).toEqual(false);

    // Email does not exist
    email.setValue('a@abcc.com');
    expect(email.valid).toEqual(true);
  });

  it('should fail if email is invalid', () => {
    const email = component.accountSignupForm.controls[FORM_FIELDS.email.name];
    email.setValue('foo@');
    expect(email.valid).toEqual(false);
  });

  it('should not fail if email is valid', () => {
    const email = component.accountSignupForm.controls[FORM_FIELDS.email.name];
    email.clearAsyncValidators();
    email.addAsyncValidators(mockValidator)
    email.setValue('foo@bar');
    expect(email.valid).toEqual(true);
  });

  it('should fail if password contains firstname', () => {
    const firstName = component.accountSignupForm.controls[FORM_FIELDS.firstName.name];
    firstName.setValue('Murat');
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('QqqqqqqMuratkasjdnkajd');
    expect(password.valid).toEqual(false);
  });

  it('should not fail if password does not contain firstname', () => {
    const firstName = component.accountSignupForm.controls[FORM_FIELDS.firstName.name];
    firstName.setValue('Murat');
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('Qqqqqqqasjdnkajd');
    expect(password.valid).toEqual(true);
  });

  it('should fail if password contains lastname', () => {
    const lastName = component.accountSignupForm.controls[FORM_FIELDS.lastName.name];
    lastName.setValue('Zengin');
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('QqqqqqqZenginakaskad');
    expect(password.valid).toEqual(false);
  });

  it('should not fail if password does not contain lastname', () => {
    const lastName = component.accountSignupForm.controls[FORM_FIELDS.lastName.name];
    lastName.setValue('Zengin');
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('FooooooooBar');
    expect(password.valid).toEqual(true);
  });

  it('should fail if password does not contain capital letter', () => {
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('foooooooobar');
    expect(password.valid).toEqual(false);
  });

  it('should not fail if password contains capital letter', () => {
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('fooooOoooobar');
    expect(password.valid).toEqual(true);
  });

  it('should contain at least 8 letters', () => {
    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];
    password.setValue('Fooobar');
    expect(password.valid).toEqual(false);
  });

  it('firstname should not be empty', () => {
    const firstName = component.accountSignupForm.controls[FORM_FIELDS.firstName.name];
    firstName.setValue('');
    expect(firstName.valid).toEqual(false);
  });

  it('lastname should not be empty', () => {
    const lastName = component.accountSignupForm.controls[FORM_FIELDS.lastName.name];
    lastName.setValue('');
    expect(lastName.valid).toEqual(false);
  });

  it('should fail if password entered first and then firstname entered in syncron', () => {

    const password = component.accountSignupForm.controls[FORM_FIELDS.password.name];

    password.setValue('FooooZenginooooBar');

    const lastName = component.accountSignupForm.controls[FORM_FIELDS.lastName.name];

    lastName.setValue('Zengin');

    expect(password.valid).toEqual(false);
  });

  it('should create form elements', () => {

    const inputs = el.queryAll(By.css('.clear-input '));

    const firstName = inputs.filter(element => element.nativeElement.placeholder === FORM_FIELDS.firstName.placeHolder);

    const lastName = inputs.filter(element => element.nativeElement.placeholder === FORM_FIELDS.lastName.placeHolder);

    const email = inputs.filter(element => element.nativeElement.placeholder === FORM_FIELDS.email.placeHolder);

    const password = inputs.filter(element => element.nativeElement.placeholder === FORM_FIELDS.password.placeHolder);

    expect(firstName && lastName && email && password).toBeTruthy();
  });

  it('should create button', () => {

    const buttons = el.queryAll(By.css('.button'));

    const button = buttons[0]?.nativeElement;

    expect(button).toBeTruthy();
  });

  it('should have disabled button if form is invalid', () => {

    const email = component.accountSignupForm.controls[FORM_FIELDS.email.name];
    email.clearAsyncValidators();
    email.addAsyncValidators(mockValidator)

    component.accountSignupForm.setValue({
      firstName: 'qqqq',
      lastName: 'qqqq',
      email: 'a@abc.com', // invalid
      password: 'FooooBar'
    });

    fixture.detectChanges();

    const buttons = el.queryAll(By.css('.button'));

    const button = buttons[0]?.nativeElement;

    const isDisabled = button.disabled;

    expect(isDisabled).toBeTruthy();
  });

  it('should have loading button if loading is active', () => {

    component.loading = true;

    fixture.detectChanges();

    const elements = el.queryAll(By.css('.loading'));

    const loading = elements[0]?.nativeElement;

    expect(loading).toBeTruthy();
  });

  it('should have create account button if loading is not active', () => {

    component.loading = false;

    fixture.detectChanges();

    const elements = el.queryAll(By.css('.create-account'));

    const createAccount = elements[0]?.nativeElement;

    expect(createAccount).toBeTruthy();
  });

  it('should fail if form is invalid', () => {

    component.accountSignupForm.controls[FORM_FIELDS.password.name].setValue('a');

    fixture.detectChanges();

    const result = component.onSubmit();

    expect(result).toBe(undefined)
  });

  it('should return success from API if form is valid', () => {

    // create an empty form as we dont need a form validation here
    component.accountSignupForm = new FormGroup({});

    const firstName = 'Murat';

    authServiceSpy.sendSignupRequest.and.returnValue(of({
      firstName,
    }));

    component.onSubmit();

    fixture.detectChanges();

    expect(component.loading).toBeFalsy();

    expect(component.message).toBe(`User ${firstName} has been created successfully!`);
  });

  it('should fail from API if form is invalid', () => {

    // create an empty form as we dont need a form validation here
    component.accountSignupForm = new FormGroup({});

    authServiceSpy.sendSignupRequest.and.returnValue(throwError(() => {
    }));

    component.onSubmit();

    expect(component.loading).toBeFalsy();

    expect(component.message).toBe(`Something went wrong!`);
  });
});

