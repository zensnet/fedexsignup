import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', // Redirect to signup as default route since there is no other route
        loadChildren: () => import('./sign-up/sign-up.module').then(m => m.SignupModule)
      },
      {
        path: 'signup',
        loadChildren: () => import('./sign-up/sign-up.module').then(m => m.SignupModule)
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
