import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthRoutingModule} from "./auth-routing.module";
import {SignupModule} from "./sign-up/sign-up.module";

@NgModule({
  imports: [
    CommonModule,
    SignupModule,
    AuthRoutingModule,
  ]
})
export class AuthModule { }
