import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ClearInputComponent} from './clear-input.component';
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";

describe('ClearInputComponent', () => {

  let el: DebugElement,
      fixture: ComponentFixture<ClearInputComponent>,
      component: ClearInputComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClearInputComponent],
      imports: [FormsModule]
    })
        .compileComponents();

    fixture = TestBed.createComponent(ClearInputComponent);

    component = fixture.componentInstance;

    el = fixture.debugElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a clear icon visible after value changes', () => {

    component.value = 'some value';

    fixture.detectChanges();

    const clearIcons = el.queryAll(By.css('.clear'));

    expect(clearIcons[0]).toBeTruthy();
  });

  it('should clear the value if clicked', () => {

    component.value = 'some value';

    fixture.detectChanges();

    const clearIcons = el.queryAll(By.css('.clear'));

    clearIcons[0].nativeElement.click();

    expect(component.value).toBe('');
  });

  it('should trigger onChanged when value changed', () => {

    component.value = 'some value';

    fixture.detectChanges();

    const clearIcons = el.queryAll(By.css('.clear'));

    clearIcons[0].nativeElement.click();

    expect(component.value).toBe('');

    spyOn(component, 'clear');

    clearIcons[0].nativeElement.click();

    expect(component.clear).toHaveBeenCalled();
  });

  it('should trigger onchange on input event', () => {

    spyOn(component, 'onChange');

    const clearIcons = el.queryAll(By.css('.clear-input'))[0]?.nativeElement;

    clearIcons.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    expect(component.onChange).toHaveBeenCalled();
  });

  it('should trigger ontouch on blur event', () => {

    spyOn(component, 'onTouched');

    const clearIcons = el.queryAll(By.css('.clear-input'))[0]?.nativeElement;

    clearIcons.dispatchEvent(new Event('blur'));

    fixture.detectChanges();

    expect(component.onTouched).toHaveBeenCalled();
  });

});
