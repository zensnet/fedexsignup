import {Component, Input, Optional, Self} from '@angular/core';
import {ControlValueAccessor, NgControl, ValidationErrors} from "@angular/forms";
import {InputErrors} from "../../interfaces/input-errors.interface";

@Component({
  selector: 'app-clear-input',
  templateUrl: './clear-input.component.html',
  styleUrls: ['./clear-input.component.css'],
})
export class ClearInputComponent implements ControlValueAccessor {

  @Input() formControlName!: string;
  @Input() inputType!: string;
  @Input() inputId!: string;
  @Input() inputName!: string;
  @Input() inputPlaceholder!: string;
  value!: string | number;
  disabled = false;

  constructor(@Self() @Optional() private control: NgControl) {
    // Setting the value accessor directly to avoid Circular DI problem.
    this.control && (this.control.valueAccessor = this);
  }

  get invalid(): boolean | null {
    return this.control ? this.control.invalid : false;
  }

  get showError(): boolean | null {
    if (!this.control) {
      return false;
    }

    const {dirty, touched} = this.control;

    return this.invalid ? (dirty || touched) : false;
  }

  get errors(): ValidationErrors | InputErrors | null {
    const {errors} = this.control;
    return errors as InputErrors;
  }

  onTouched = () => {
  };

  onChange = (_: any) => {
  };

  clear(): void {
    this.value = '';
    this.onChange(this.value);
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: () => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }
}
