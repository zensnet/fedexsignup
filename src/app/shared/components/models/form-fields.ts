import {FormFields} from "../../interfaces/form-fields";

export const FORM_FIELDS: FormFields = {
    firstName: {
        text: 'First Name',
        name: 'firstName',
        type: 'text',
        placeHolder: 'Your first name'
    },
    lastName: {
        text: 'Last Name',
        name: 'lastName',
        type: 'text',
        placeHolder: 'Your last name'
    },
    password: {
        text: 'Password',
        name: 'password',
        type: 'password',
        placeHolder: 'Your password'
    },
    email: {
        text: 'Email',
        name: 'email',
        type: 'email',
        placeHolder: 'Your email'
    }
}
