import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClearInputComponent} from "./components/clear-input/clear-input.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [ClearInputComponent],
  exports: [
    ClearInputComponent
  ],

  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ]
})
export class SharedModule {
}
