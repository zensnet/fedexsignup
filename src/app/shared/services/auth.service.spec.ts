import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {AuthService} from './auth.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {User} from "../interfaces/user.interface";
import {HttpErrorResponse} from "@angular/common/http";

describe('AuthServiceService', () => {
  let authService: AuthService,
      httpTestingController: HttpTestingController,
      user: User;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService],
      imports: [HttpClientTestingModule]
    });

    authService = TestBed.get(AuthService);
    httpTestingController = TestBed.get(HttpTestingController);

    user = {
      email: 'murat@zensnet.com',
      firstName: 'Murat',
      lastName: 'Zengin',
    }
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should have POST method', () => {

    authService.sendSignupRequest(user).subscribe(response => {
      expect(response).toBeTruthy();
    });

    const req = httpTestingController.expectOne('https://demo-api.now.sh/users');
    expect(req.request.method).toBe('POST');
    req.flush({});
  });

  it('should have return type of user', () => {

    authService.sendSignupRequest(user).subscribe(response => {
      expect(response).toBeInstanceOf(user.constructor)
    });

    const req = httpTestingController.expectOne('https://demo-api.now.sh/users');

    req.flush({});
  });

  it('should return false if email exists', fakeAsync(() => {
    let result = true;

    const email: string = 'a@abc.com';

    authService.checkEmail(email).subscribe(response => {
      result = response;
    });

    // simulating the delay in the service api using fakeAsync
    setTimeout(() => {
    }, 500);

    tick(500);

    expect(result).toBe(true);
  }))

  it('should return an output when status is 200', () => {

    // Always returns same result so use the same
    const payload: User = {"email": "janedoe@apple.com", "firstName": "Jane", "lastName": "Doe"};

    // Looks kind of an integration test here,
    // but we aim to test here a valid function rather than a valid payload
    authService.sendSignupRequest(user).subscribe(response => {
      if (response) {
        expect(response.firstName).toBe('Jane');
      }
    });

    const req = httpTestingController.expectOne('https://demo-api.now.sh/users');

    expect(req.request.method).toBe('POST');

    req.flush(payload, {status: 200, statusText: 'OK'});
  });

  it('should handle undefined when status 404', () => {

    let responseUser: User | undefined;

    authService.sendSignupRequest(user).subscribe(response => {
      responseUser = response;
    });

    const req = httpTestingController.expectOne('https://demo-api.now.sh/users');

    req.flush({}, {status: 404, statusText: 'NOT FOUND'});

    expect(responseUser).toBeUndefined();
  });

  it('should throw error when status other than 404', () => {

    let errorResponse: HttpErrorResponse = new HttpErrorResponse({status: 400});

    authService.sendSignupRequest(user).subscribe(response => {
        },
        error => {
          errorResponse = error;
        });

    const req = httpTestingController.expectOne('https://demo-api.now.sh/users');

    req.flush({}, {status: 500, statusText: 'Internal Server Error'});

    expect(errorResponse.status).toBe(500)
  });
});
