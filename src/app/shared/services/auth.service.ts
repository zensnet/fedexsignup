import {Injectable} from '@angular/core';
import {catchError, delay, map, Observable, of, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {User} from "../interfaces/user.interface";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    url: string = 'https://demo-api.now.sh';

    constructor(private http: HttpClient) {
    }

    sendSignupRequest(user: User): Observable<User | undefined> {
        return this.http.post<User>(`${this.url}/users`, user).pipe(
            map(result => result),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 404) {
                    return of(undefined);
                } else {
                    return throwError(() => error);
                }
            })
        )
    }

    checkEmail(email: string): Observable<boolean> {
        return of(email === 'a@abc.com').pipe(
            delay(500), // adding a bit of delay to simulate http api call
            map(result => result)
        )
    }
}
