import {FieldDefinitions} from "./models/field-definitions";

export interface FormFields {
    firstName: FieldDefinitions;
    lastName: FieldDefinitions;
    password: FieldDefinitions;
    email: FieldDefinitions;
}
