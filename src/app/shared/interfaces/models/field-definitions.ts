export interface FieldDefinitions {
    text: string,
    name: string,
    type: string,
    placeHolder: string
}
