export interface InputErrors {
    required?: boolean;
    email?: boolean;
    emailExists?: boolean;
    passwordContainsName?: boolean;
    minlength?: boolean;
    passwordStrength?: boolean;
}
