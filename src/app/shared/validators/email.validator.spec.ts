import {TestBed} from "@angular/core/testing";
import {AuthService} from "../services/auth.service";
import {EmailValidator} from "./email.validator";
import {FormControl} from "@angular/forms";
import {of} from "rxjs";

describe('EmailValidator', () => {
    let authService: AuthService,
        emailValidator: EmailValidator,
        authServiceSpy: any;
    beforeEach(() => {
        authServiceSpy = jasmine.createSpyObj('AuthService', ['checkEmail']);
        TestBed.configureTestingModule({
            providers: [
                EmailValidator,
                {
                    provide: AuthService, useValue: authServiceSpy
                }
            ]
        });

        authService = TestBed.get(AuthService);
        emailValidator = TestBed.get(EmailValidator);
    })

    it('should be created', () => {
        expect(emailValidator).toBeTruthy();
    });

    it('should return false if api returns finds a matching email', () => {

        // mock the checkEmail function of AuthService api
        authServiceSpy.checkEmail.and.returnValue(of(true));

        // simulate form and async validator
        const formControl = new FormControl();
        formControl.addAsyncValidators(emailValidator.checkEmail());
        formControl.updateValueAndValidity();

        expect(formControl.valid).toBeFalsy();
    })
})
