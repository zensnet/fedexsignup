import {AbstractControl, AsyncValidatorFn} from '@angular/forms';
import {AuthService} from "../services/auth.service";
import {map} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class EmailValidator {
    constructor(private authService: AuthService) {
    }

    checkEmail(): AsyncValidatorFn {
        return (control: AbstractControl): any => {
            return this.authService.checkEmail(control.value)
                .pipe(
                    map(result => {
                        return result ? {emailExists: true} : null;
                    })
                )
        }
    }
}
