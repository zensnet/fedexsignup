import {AbstractControl, ValidatorFn} from '@angular/forms';
import {Injectable} from "@angular/core";
import {FORM_FIELDS} from "../components/models/form-fields";

@Injectable({
    providedIn: 'root'
})
export class PasswordValidator {
    checkStrength(): ValidatorFn {
        return (passwordField: AbstractControl): any => {

            const passwordRegex = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])/, 'g');

            let valid: boolean = passwordRegex.test(passwordField.value);

            return valid ? null : {passwordStrength: true};
        }
    }
    checkPassword(): ValidatorFn {
        return (formGroup: AbstractControl): any => {
            const passwordField = formGroup?.get(FORM_FIELDS.password.name);

            const firstNameField: AbstractControl | null = formGroup?.get(FORM_FIELDS.firstName.name);

            const lastNameField: AbstractControl | null = formGroup?.get(FORM_FIELDS.lastName.name);

            let valid: boolean = true;

            if (firstNameField?.value) {
                valid = passwordField?.value?.toLowerCase().indexOf(firstNameField.value?.toLowerCase()) === -1;
            }

            if (lastNameField?.value) {
                valid &&= passwordField?.value?.toLowerCase().indexOf(lastNameField.value?.toLowerCase()) === -1;
            }

            if ((firstNameField?.value || lastNameField?.value) && !valid) {
                passwordField?.setErrors({...passwordField?.errors, ...{passwordContainsName: true}});

            } else {
                if(passwordField?.errors) {
                    let {passwordContainsName, ...others} = passwordField?.errors as any;

                    if(Object.keys(others).length === 0){
                        others = null;
                    }
                    passwordField?.setErrors(others);
                }
            }

            return valid ? null : {passwordContainsName: true};
        }
    }
}

