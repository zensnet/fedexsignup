import {ComponentFixture, TestBed} from "@angular/core/testing";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {PasswordValidator} from "./password.validator";
import {FORM_FIELDS} from "../components/models/form-fields";

describe('PasswordValidator', () => {
    let passwordValidator: PasswordValidator,
        formBuilder: FormBuilder,
        form: FormGroup;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule],
            providers: [
                PasswordValidator,
            ]
        });

        passwordValidator = TestBed.get(PasswordValidator);

        formBuilder = TestBed.get(FormBuilder);

        // simulate form and add validators
        form = formBuilder.group({
            firstName: [''],
            lastName: [''],
            password: ['', [Validators.required, passwordValidator.checkStrength(), Validators.minLength(8)]]
        }, {
            validators: [passwordValidator.checkPassword()],
        });
    })

    it('should be created', () => {
        expect(passwordValidator).toBeTruthy();
    });

    it('should return invalid state if password contains firstname/lastname', () => {

        form.controls[FORM_FIELDS.firstName.name].setValue('Murat');
        form.controls[FORM_FIELDS.lastName.name].setValue('Zengin');
        form.controls[FORM_FIELDS.password.name].setValue('QQQQMurat');

        expect(form.valid).toBeFalsy();

    })

    it('should return invalid state if password has no capital letters', () => {

        form.controls[FORM_FIELDS.password.name].setValue('foobar');

        expect(form.valid).toBeFalsy();

    })

    it('should return invalid state if password has less than 8 chars', () => {

        form.controls[FORM_FIELDS.password.name].setValue('Foo');

        expect(form.valid).toBeFalsy();

    })

    it('should return invalid state if password empty', () => {

        expect(form.valid).toBeFalsy();

    })
})
